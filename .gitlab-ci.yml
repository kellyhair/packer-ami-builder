# This file is a template, and might need editing before it works on your project.
image:
  name: hashicorp/packer:latest
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

before_script:
  - packer --version

# ---- Stages -----

stages:
  - Validation
  - Build Base 
  - Build Children
  - Deploy Multiple AWS Regions
  - Weekly Build Base
  - Weekly Builds

# ---- Validation -----

validate:
  stage: Validation
  except:
    - schedules
  script:
    - find . -maxdepth 3 -name 'packer*.json' -print0 | xargs -t0n1 packer validate

# ---- AMI Builds - Base Images Go in in this Stage -----

build:DockerCE:
  stage: Build Base
  environment: us-east-2
  except:
    - schedules
  script:
    - packer build ./Docker-CE/packer-docker.json
  only:
    changes:
      - Docker-CE/**/*
      - GitLab-Runner-Docker/**/*

build:DockerCE-ARM64:
  stage: Build Base 
  environment: us-east-1
  except:
    - schedules
  script:
    - packer build ./Docker-CE-ARM64/packer-docker.json
  only:
    changes:
      - Docker-CE-ARM64/**/*
      - GitLab-Runner-Docker-ARM64/**/*


build:GitLabEE-PW:
  stage: Build Base
  environment: us-east-2
  except:
    - schedules
  script:
    - packer build ./GitLab-EE-Password-Set/packer-GitLab-EE-Password-Set.json
  only:
    changes:
      - GitLab-EE-Password-Set/**/*


build:OpenLDAP:
  stage: Build Base
  environment: us-east-2
  except:
    - schedules
  script:
    - packer build ./OpenLDAP/packer-OpenLDAP.json
  only:
    changes:
      - OpenLDAP/**/*

build:Dedicated_GitLab_Runner_no_Docker:
  stage: Build Base
  environment: us-east-2
  except:
    - schedules
  script:
    - packer build ./GitLab-Runner-No-Docker/packer-runner.json
  only:
    changes:
      - GitLab-Runner-No-Docker/**/*


build:Node:
  stage: Build Base
  environment: us-east-2
  except:
    - schedules
  script:
    - packer build ./Node/packer-nodejs.json
  only:
    changes:
      - Node/**/*


# ---- AMI Builds for Docker Dependencies -----

build:Dedicated_GitLab_Runner_Docker:
  stage: Build Children
  needs: ["build:DockerCE"]
  environment: us-east-2
  except:
    - schedules
  script:
    - packer build ./GitLab-Runner-Docker/packer-runner.json
  only:
    changes:
      - GitLab-Runner-Docker/**/*
      - Docker-CE/**/*

build:GitLab_Runner_ARM64_Docker:
  stage: Build Children
  needs: ["build:DockerCE-ARM64"]
  environment: us-east-1
  except:
    - schedules
  script:
    - packer build ./GitLab-Runner-Docker-ARM64/packer-runner.json
  only:
    changes:
      - Docker-CE-ARM64/**/*
      - GitLab-Runner-Docker-ARM64/**/*


# ---- Copying AMIs to other regions -----

deploy:GitLab_EE_PW-multi:
  stage: Deploy Multiple AWS Regions
  environment: us-east-1 _ us-west-2 _ eu-west-1
  except:
    - schedules
  script:
    - packer build ./GitLab-EE-Password-Set/multi_region/packer-GitLab-EE-PW-multi.json
  dependencies:
    - build:GitLabEE-PW
  only:
    changes:
      - GitLab-EE/**/*
    refs:
      - master
  when: manual

deploy:OpenLDAP_multi:
  stage: Deploy Multiple AWS Regions
  environment: us-east-1 _ us-west-2 _ eu-west-1
  except:
    - schedules
  script:
    - packer build ./OpenLDAP/multi_region/packer-OpenLDAP-multi.json
  dependencies:
    - build:OpenLDAP
  only:
    changes:
      - OpenLDAP/**/*
    refs:
      - master
  when: manual

deploy:Dedicated_GitLab_Runner_no_Docker_multi:
  stage: Deploy Multiple AWS Regions
  environment: us-east-1 _ us-west-2 _ eu-west-1
  except:
    - schedules
  script:
    - packer build ./GitLab-Runner-No-Docker/multi_region/packer-Dedicated_GitLab_Runner_no_Docker-multi.json
  dependencies:
    - build:Dedicated_GitLab_Runner_no_Docker
  only:
    changes:
      - GitLab-Runner-No-Docker/**/*
    refs:
      - master
  when: manual

deploy:Dedicated_GitLab_Runner_Docker_multi:
  stage: Deploy Multiple AWS Regions
  environment: us-east-1 _ us-west-2 _ eu-west-1
  except:
    - schedules
  script:
    - packer build ./GitLab-Runner-Docker/multi_region/packer-Dedicated_GitLab_Runner_Docker-multi.json
  dependencies:
    - build:Dedicated_GitLab_Runner_Docker
  only:
    changes:
      - GitLab-Runner-Docker/**/*
    refs:
      - master
  when: manual



deploy:Node_multi:
  stage: Deploy Multiple AWS Regions
  environment: us-east-1 _ us-west-2 _ eu-west-1
  except:
    - schedules
  script:
    - packer build ./Node/multi_region/packer-Node-multi.json
  dependencies:
    - build:Node
  only:
    changes:
      - Node/**/*
    refs:
      - master
  when: manual


deploy:DockerCE_multi:
  stage: Deploy Multiple AWS Regions
  environment: us-east-1 _ us-west-2 _ eu-west-1
  except:
    - schedules
  script:
    - packer build ./Docker-CE/multi_region/packer-docker-multi.json
  dependencies:
    - build:DockerCE
  only:
    changes:
      - Docker-CE/**/*
    refs:
      - master
  when: manual

# ---- Weekly build & deploy to ALL AWS regions -----

# Since Runner_Docker depends on Docker CE build this first and since the EE takes time, kick that off too:

weekly:Docker-CE:
  stage: Weekly Build Base
  only:
    - schedules
    - master
  script:
    - packer build ./Docker-CE/weekly/packer-docker-weekly.json


weekly:GitLab-EE-PW:
  stage: Weekly Build Base
  only:
    - schedules
    - master
  script:
    - packer build ./GitLab-EE-Password-Set/weekly/packer-gitlab-ee-pw.json

# Remaining Weekly builds. Uses DAG to help speed up build process. 
# See: https://docs.gitlab.com/ee/ci/yaml/README.html#needs


weekly:Runner_Docker:
  stage: Weekly Builds
  needs: ["weekly:Docker-CE"]
  only:
    - schedules
    - master
  script:
    - packer build ./GitLab-Runner-Docker/weekly/packer-runner_docker.json
