#!/bin/bash

url="https://gitlab.com/" # URL of GitLab instance
executor="docker" #Type of Runner
token="`cat /root/token.txt`"
description="gitlab-ec2-docker-runner-`date +%Y%m%d-%H%M%S`-`rand`" #Runner name
taglist="aws,ec2,gitlab,docker" #Tags to add to GitLab GUI
image=alpine:latest
# -------- Script -------
sudo gitlab-runner register \
  --non-interactive \
  --url $url \
  --registration-token $token \
  --executor $executor \
  --description $description \
  --tag-list $taglist \
  --run-untagged \
  --locked="false" \
  --docker-image $image \
